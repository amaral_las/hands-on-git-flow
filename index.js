const express = require('express')
const { environment } = require('./environment/dev')
const router = require('./commons/router')
const app = express()
const port = environment.port

app.use("", router)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})